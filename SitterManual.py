import sys
from Utils import load_text_from_args
from Tweet import STweet
from NewSitter import Sitter


class SitterManual(object):
    sitter = None
    counter = 0
    def __init__(self):
        best_c1 = 0.1
        best_c2 = 0.001
        best_c3 = 0
        self.sitter = Sitter()
        self.sitter.set_cparams(best_c1, best_c2, best_c3)
        self.sitter.setup_query("manual", 0)

    def sitter_manual_detection(self):
        try:
            buffer = ""
            print "waiting for input.."
            while True:
                buffer += sys.stdin.read(1)
                if buffer.endswith("\n"):
                    tweetobject = STweet()
                    tweetobject.setOriginText(buffer)
                    if Sitter is not None:
                        self.sitter.detect_sentiments([tweetobject], False)
                        if self.sitter.processed[self.counter].prediction == 0:
                            print "NEGATIVE (%d) : %s" % (self.sitter.processed[self.counter].confidence, buffer)
                        elif self.sitter.processed[self.counter].prediction == 1:
                            print "POSITIVE (%d) : %s" % (self.sitter.processed[self.counter].confidence, buffer)
                        else:
                            print "NEUTRAL: " + buffer

                        self.counter += 1
                        break
                    else:
                        print "fatal error: sitter not ready"
            self.sitter_manual_detection()
        except KeyboardInterrupt:
            print "bye"


manual = SitterManual()
manual.sitter_manual_detection()
