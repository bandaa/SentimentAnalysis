from Tweet import Tweet
from random import shuffle
from Lexicons.MinquingLexicon import MinquingLexicon
from Lexicons.AffinLexicon import AffinLexicon
from Lexicons.NRCLexicon import NRCLexicon
from Lexicons.NegationLexicon import NegationLexicon
from Lexicons.MpqaLexicon import MpqaLexicon
from Lexicons.NRCLexiconExtra import NRCLexiconExtra, NRCExtraItem

minqing_path = "Lexicons/Minqinghu/"
affin_path = "Lexicons/Affin/"


class Lexicons(object):
    lexicons = []

    def load(self):
        if self.lexicons is None or len(self.lexicons) != 0:
            return
        self.lexicons.append(MinquingLexicon())
        self.lexicons.append(AffinLexicon())
        self.lexicons.append(NRCLexicon())
        self.lexicons.append(NegationLexicon())
        self.lexicons.append(MpqaLexicon())
        self.lexicons.append(NRCLexiconExtra())

    def getLexiconsFeatures(self, tweet):
        features = []
        for lexicon in self.lexicons:
            features.extend(lexicon.get_lexicon_features(tweet.tokens, tweet.bigrams))

        return features


def get_feature_vectors(file_name, ignore, separator, default, not_binary, sentiment_index, data_index):
    feature_vectors = []

    file = open(file_name, "r")
    for line in file:
        try:
            if ignore is None or not line.startswith(ignore):
                if separator is not None:
                    stripped = line.strip(separator).split()
                else:
                    stripped = line.strip().split()

                if len(stripped) > 0:
                    if len(stripped) > 1:
                        if not_binary:
                            feature_vectors.append(Tweet(None, stripped[data_index], float(stripped[sentiment_index])))
                        else:
                            if float(stripped[sentiment_index]) < 0:
                                feature_vectors.append(Tweet(None, stripped[data_index], 0))
                            else:
                                feature_vectors.append(Tweet(None, stripped[data_index], 1))
                    else:
                        feature_vectors.append(Tweet(None, stripped[data_index], default))
        except Exception as e:
            pass
            # print "Skip line because: " + e.message
    return feature_vectors


def get_lexicon_listitems(file_name, word_separator, basic_separator, word_indexes, sentiment_index):
    items = {}
    file = open(file_name, "r")
    for line in file:
        try:
            if basic_separator is not None:
                stripped = line.strip().split(basic_separator)
            else:
                stripped = line.strip().split()

            if len(stripped) > 0:
                if word_separator is None:
                    words = (stripped[0], stripped[1])
                    items[words] = float(stripped[sentiment_index])
                else:
                    pass
                    # TODO

        except Exception as e:
            pass
    return items
