import datetime
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

file_name = "Statistics/Tweets/statistic_" + str(datetime.datetime.now().strftime('%Y-%m-%d-%H_%M')) + ".png"
kernel_file_name = "Statistics/Tweets/kernel_statistic_" + str(
    datetime.datetime.now().strftime('%Y-%m-%d-%H_%M')) + ".png"


def drawPie(colors, sizes, labels, title):
    plt.pie(
        x=sizes,
        shadow=True,
        colors=colors,
        labels=labels,
        startangle=90
    )

    plt.title(title)
    plt.savefig(file_name)
    plt.close()
    print 'Diagram saved'


def drawKernel(data_set, sentiments, title):
    pca = PCA(n_components=2).fit(data_set)
    pca_2d = pca.transform(data_set)

    for i in range(0, pca_2d.shape[0]):
        if sentiments[i] == '0':
            c1 = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='r', marker='+')
        elif sentiments[i] == '1':
            c2 = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='g', marker='+')
    plt.legend([c1, c2], ['Negative', 'Positive'])
    plt.title(title)
    plt.savefig(kernel_file_name)
    plt.close()
    print 'Kernel diagram saved'
