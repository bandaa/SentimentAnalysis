import numpy as np
from requests import post


class Tweet(object):

    def __init__(self, id, text, sentiment):
        self.id = id
        self.text = text
        self.sentiment = sentiment


class STweet(object):

    def setOriginText(self, text):
        self.origin = text

    def setText(self, text):
        self.text = text

    def setSentiment(self, sentiment):
        self.sentiment = sentiment

    def setPrediction(self, sentiment):
        self.prediction = sentiment

    def setTokens(self, tokens):
        self.tokens = tokens

    def setPostags(self, postags):
        self.postags = postags

    def setUnigrams(self, unigrams):
        self.unigrams = unigrams

    def setBiagrams(self, biagrams):
        self.biagrams = biagrams

    def setTriagrams(self, triagrams):
        self.triagrams = triagrams

    def setFeatures(self, features):
        self.features = features

    def setConfidence(self, confidence):
        self.confidence = confidence

    def setProcessedPostags(self, processed_postags):
        self.processed_tags = processed_postags

    def setBigrams(self, bigrams):
        self.bigrams = bigrams

    def setTrigrams(self, trigrams):
        self.triagrams = trigrams

    def setScores(self, postag_scores, bigrams_scores, trigrams_scores):
        self.postag_score = postag_scores
        self.bigrams_scores = bigrams_scores
        self.triagrams_scores = trigrams_scores

    def __str__(self):
        tweet_string = "{\n"
        if hasattr(self, 'origin'):
            pass
            tweet_string += ("Origin: " + str(self.origin.encode('utf-8')) + "\n")
        if hasattr(self, 'text'):
            tweet_string += ("Text: " + str(self.text.encode('utf-8')) + "\n")
        if hasattr(self, 'sentiment'):
            tweet_string += ("Sentiment: " + str(self.sentiment) + "\n")
        if hasattr(self, 'prediction'):
            tweet_string += ("Prediction: " + str(self.prediction) + "\n")
        if hasattr(self, 'tokens'):
            tweet_string += ("Tokens: " + str(self.tokens) + "\n")
        if hasattr(self, 'features'):
            tweet_string += ("Features: " + str(self.features) + "\n")
        if hasattr(self, 'confidence'):
            tweet_string += ("Confidence: " + str(self.confidence) + "\n")

        tweet_string += "}"
        return tweet_string

    def get_as_dictionary(self):
        thiz = {}
        if hasattr(self, 'origin'):
            try:
                thiz.update({"Origin": self.origin.encode('utf-8')})
            except:
                pass
        if hasattr(self, 'sentiment'):
            try:
                thiz.update({"Sentiment": self.sentiment})
            except:
                pass
        if hasattr(self, 'confidence'):
            try:
                thiz.update({"Confidence": self.confidence})
            except:
                pass
        if hasattr(self, 'prediction'):
            try:
                thiz.update({"Prediction": self.prediction})
            except:
                pass
        if hasattr(self, 'tokens'):
            try:
                thiz.update({"Tokens": self.tokens})
            except:
                pass
        if hasattr(self, 'features'):
            try:
                thiz.update({"Features": self.features})
            except:
                pass
        if hasattr(self, 'text'):
            try:
                thiz.update({"Processed": self.text.encode('utf-8')})
            except:
                pass

        return thiz


def convert_tweet_to_stweet(tweet):
    stweet = STweet()
    if hasattr(tweet, 'text') and tweet.text is not None:
        stweet.setOriginText(tweet.text)
    if hasattr(tweet, 'sentiment') and tweet.sentiment is not None:
        stweet.setSentiment(tweet.sentiment)
    return stweet


def get_all_text_asnp(tweets):
    texts = []
    for tw in tweets:
        texts.append(tw.text)
    return np.array(texts)


def get_all_sentiment_asnp(tweets):
    sent = []
    for tw in tweets:
        sent.append(tw.sentiment)
    return np.array(sent)


def get_all_tokens_asnp(tweets):
    tokens = []
    for tw in tweets:
        tokens.append(tw.tokens)
    return np.array(tokens)


def get_all_feature_asnp(tweets):
    feat = []
    for tw in tweets:
        feat.append(tw.features)
    return np.array(feat)


def check_tweet(tw):
    try:
        if tw.text is None or tw.tokens is None or tw.features is None:
            return None
        else:
            return tw
    except Exception as e:
        return None


def check_tweet2(tw):
    try:
        if tw.text is None or tw.tokens is None or tw.features is None or tw.confidence is None:
            return None
        else:
            return tw
    except Exception as e:
        return None


def check_tweets(tweets):
    for tw in tweets:
        try:
            if tw.text is None or tw.tokens is None or tw.features is None:
                tweets.remove(tw)
        except Exception as e:
            tweets.remove(tw)


def check_tweets2(tweets):
    for tw in tweets:
        try:
            if tw.text is None or tw.tokens is None or tw.features is None or tw.confidence is None or tw.prediction is None:
                tweets.remove(tw)
        except Exception as e:
            tweets.remove(tw)


def refreshFeatures(data, features):
    if len(data) != len(features):
        print 'ERROR: Data length (%d) != features length (%d)' % (len(data), len(features))
        return

    for i in range(0, len(data)):
        data[i].setFeatures(features[i])


def refreshConfidences(data, confidences):
    if len(data) != len(confidences):
        print 'ERROR: Data length (%d) != confidences length (%d)' % (len(data), len(confidences))
        return

    for i in range(0, len(confidences)):
        data[i].setConfidence(confidences[i])


def concatenate_final_set(data):
    final = []
    for d in data:
        s = set(d.features)
        final.append((s, d.sentiment))
    return final
