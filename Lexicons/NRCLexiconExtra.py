import SitterLexicons
import collections


class NRCExtraItem(object):
    words = set()
    sentiment = 0

    def __init__(self, words, sentiment):
        self.words = words
        self.sentiment = sentiment


class NRCLexiconExtra(object):
    path = "Lexicons/NRC/"
    data = []
    res = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (self.get_lexicon_size(), self.__class__.__name__)
        self.set_res()

    def set_res(self):
        self.res = []
        for d in self.data:
            # pos, neg, summ
            self.res.extend([0.0])

    def get_lexicon_size(self):
        lenght = 0
        for l in self.data:
            lenght += len(l)
        return lenght

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vectors = []

        # Be carefull, because this will eat the ram af
        feature_vectors.append(
            SitterLexicons.get_lexicon_listitems(self.path + "Aff/bigrams.txt", None, None, [0, 1], 2))
        feature_vectors.append(
            SitterLexicons.get_lexicon_listitems(self.path + "HashtagNRC/bigrams-pmilexicon.txt", None, None, [0, 1],
                                                 2))
        feature_vectors.append(
            SitterLexicons.get_lexicon_listitems(self.path + "Hashtags/bigrams.txt", None, None, [0, 1], 2))
        feature_vectors.append(
            SitterLexicons.get_lexicon_listitems(self.path + "Pmil/bigrams-pmilexicon.txt", None, None, [0, 1], 2))

        self.data = feature_vectors

    def get_lexicon_features(self, tokens, bigrams):
        self.set_res()
        for i in range(0, len(self.data)):
            for bi in bigrams:
                self.res[i] += self.data[i].get(bi, 0.0)
        return self.res

    def get_data(self):
        return self.data
