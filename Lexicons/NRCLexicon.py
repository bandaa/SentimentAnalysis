import SitterLexicons


class NRCLexicon(object):
    path = "Lexicons/NRC/"
    data = []
    res = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (self.get_lexicon_size(), self.__class__.__name__)
        self.set_res()

    def set_res(self):
        self.res = []
        for d in self.data:
            # pos, neg, summ
            self.res.extend([0.0, 0.0, 0.0])

    def get_lexicon_size(self):
        lenght = 0
        for l in self.data:
            lenght += len(l)
        return lenght

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vectors = []

        feature_vectors.append(
            SitterLexicons.get_feature_vectors(self.path + "Hashtags/unigrams.txt", '\t', None, None, True, 1, 0))
        feature_vectors.append(
            SitterLexicons.get_feature_vectors(self.path + "Maxdiff/Maxdiff-Twitter-Lexicon_-1to1.txt", None, None,
                                               None, True, 0, 1))
        feature_vectors.append(
            SitterLexicons.get_feature_vectors(self.path + "Pmil/unigrams-pmilexicon.txt", None, None, None, True, 1,
                                               0))
        feature_vectors.append(
            SitterLexicons.get_feature_vectors(self.path + "Aff/unigrams.txt", None, None, None, True, 1, 0))
        feature_vectors.append(
            SitterLexicons.get_feature_vectors(self.path + "HashtagNRC/unigrams-pmilexicon.txt", None, None, None, True,
                                               1, 0))
        self.data = feature_vectors

    def get_lexicon_features(self, tokens, bigrams):
        self.set_res()
        for i in range(0, len(self.data)):
            lex = self.data[i]
            for d in lex:
                for t in tokens:
                    if d.text == t:
                        self.res[i + 2] += d.sentiment
                        if d.sentiment < 0:
                            self.res[i + 1] += d.sentiment
                        elif d.sentiment > 0:
                            self.res[i + 0] += d.sentiment
        return self.res

    def get_data(self):
        return self.data
