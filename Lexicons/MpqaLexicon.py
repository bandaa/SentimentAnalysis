import SitterLexicons


class MpqaElement(object):
    type = None
    word = None
    pos = None
    sentiment = None
    score = 0.0


class MpqaLexicon(object):
    path = "Lexicons/Mpqa/"
    data = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (len(self.data), self.__class__.__name__)

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vectors = []

        file = open(self.path + "mpqa.tff", "r")
        for line in file:
            try:
                stripped = line.strip(" ").split()
                if len(stripped) > 0:
                    element = MpqaElement()
                    for item in stripped:
                        if item.startswith("priorpolarity"):
                            element.sentiment = item.split("=")[1]
                        elif item.startswith("type"):
                            element.type = item.split("=")[1]
                        elif item.startswith("word1"):
                            element.word = item.split("=")[1]
                        elif item.startswith("pos1"):
                            element.pos = item.split("=")[1]
                    if element.type.startswith("weak"):
                        element.score = 0.5 if element.sentiment == "positive" else (
                            -0.5 if element.sentiment == "negative" else 0)
                    elif element.type.startswith("strong"):
                        element.score = 1 if element.sentiment == "positive" else (
                            -1 if element.sentiment == "negative" else 0)
                    feature_vectors.append(element)
            except Exception as e:
                pass

        self.data = feature_vectors

    def get_lexicon_features(self, tokens, bigrams):
        # pos, neg, summ
        res = [0.0, 0.0, 0.0]
        for d in self.data:
            for t in tokens:
                if d.word == t:
                    res[2] += d.score
                    if d.score < 0:
                        res[1] += d.score
                    elif d.score > 0:
                        res[0] += d.score
        return res

    def get_data(self):
        return self.data
