import SitterLexicons


class MinquingLexicon(object):
    path = "Lexicons/Minqinghu/"
    data = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (len(self.data), self.__class__.__name__)

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vector = []

        feature_vector.extend(
            SitterLexicons.get_feature_vectors(self.path + "positive-words.txt", ';', None, 1, True, 1, 0))
        feature_vector.extend(
            SitterLexicons.get_feature_vectors(self.path + "negative-words.txt", ';', None, 0, True, 1, 0))

        self.data = feature_vector

    def get_lexicon_features(self, tokens, bigrams):
        # pos, neg
        res = [0, 0, 0]
        for d in self.data:
            for t in tokens:
                if d.text == t:
                    if d.sentiment < 0:
                        res[1] += 1
                        res[2] -= 1
                    elif d.sentiment > 0:
                        res[0] += 1
                        res[2] += 1
        return res

    def get_data(self):
        return self.data
