import SitterLexicons


class AffinLexicon(object):
    path = "Lexicons/Affin/"
    data = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (len(self.data), self.__class__.__name__)

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vectors = []

        feature_vectors.extend(
            SitterLexicons.get_feature_vectors(self.path + "AFINN-96.txt", None, None, None, True, 1, 0))
        feature_vectors.extend(
            SitterLexicons.get_feature_vectors(self.path + "AFINN-111.txt", None, None, None, True, 1, 0))
        feature_vectors.extend(
            SitterLexicons.get_feature_vectors(self.path + "AFINN-emoticon-8.txt", None, None, None, True, 1, 0))
        feature_vectors.extend(
            SitterLexicons.get_feature_vectors(self.path + "AFINN-en-165.txt", None, None, None, True, 1, 0))

        self.data = feature_vectors

    def get_lexicon_features(self, tokens, bigrams):
        # pos, neg, summ
        res = [0.0, 0.0, 0.0]
        for d in self.data:
            for t in tokens:
                if d.text == t:
                    res[2] += d.sentiment
                    if d.sentiment < 0:
                        res[1] += d.sentiment
                    elif d.sentiment > 0:
                        res[0] += d.sentiment
        return res

    def get_data(self):
        return self.data
