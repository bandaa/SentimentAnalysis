import SitterLexicons


class NegationLexicon(object):
    path = "Lexicons/Negations/"
    data = []

    def __init__(self):
        self.load()
        print "%d data loaded for lexicon: %s" % (len(self.data), self.__class__.__name__)

    def load(self):
        if self.data is None or len(self.data) != 0:
            return
        feature_vector = []

        feature_vector.extend(
            SitterLexicons.get_feature_vectors(self.path + "negations.txt", None, None, 0, True, 1, 0))

        self.data = feature_vector

    def get_lexicon_features(self, tokens, bigrams):
        # has negation
        res = [0]
        for d in self.data:
            for t in tokens:
                if d.text == t:
                    res[0] += 1
        return res

    def get_data(self):
        return self.data
