# coding=utf-8
import re
from sklearn.cross_validation import KFold
from SitterClassifier import train_svm
from PostagUtils import F1PosTagsScore, F1PosBigramsScore, F1PosTrigramsScore, getScorces, saveScores, loadScores, \
    clearScores


def getFeatures(tweets, lexicons):
    try:
        scorces = getScorces(tweets)
        clearScores()
        saveScores(scorces)
    except Exception as e:
        scorces = loadScores()

    for tw in tweets:
        try:
            f = calculate_features(tw, lexicons, scorces)
            if f is not None:
                tw.setFeatures(f)
            else:
                tweets.remove(tw)
        except Exception as e:
            tweets.remove(tw)
            pass


def calculate_features(tweet, lexicons, scorces):
    f = []
    f.append(num_capitalized_words(tweet))
    f.append(num_partlycapitalized_words(tweet))
    f.append(num_elongted_words(tweet))
    f.append(number_of_character(tweet, '!'))
    f.append(number_of_only_character(tweet, '!'))
    f.append(number_of_character(tweet, '?'))
    f.append(number_of_only_character(tweet, '?'))
    f.append(number_of_character(tweet, '.'))
    f.append(number_of_only_character(tweet, '.'))
    f.append(number_of_character(tweet, '^'))
    f.append(number_of_only_character(tweet, '^'))

    # there is the full list: https://stackoverflow.com/questions/15388831/what-are-all-possible-pos-tags-of-nltk
    f.append(number_of_postag("JJ", tweet))  # melléknév (első fok)
    f.append(number_of_postag("JJR", tweet))  # melléknév (középső fok)
    f.append(number_of_postag("JJS", tweet))  # melléknév (felső fok)
    f.append(number_of_postag("RB", tweet))  # határozó szó (alapfok)
    f.append(number_of_postag("RBR", tweet))  # határozó szó (közpfok)
    f.append(number_of_postag("RBS", tweet))  # határozószó (felsőfok)
    f.append(number_of_postag("IN", tweet))  # prepozíció v konjukció
    f.append(number_of_postag("MB", tweet))  # moduláris segédeszköz (kéne, fogom ...stb - szívesen :))
    f.append(number_of_postag("UH", tweet))  # indulatszó
    f.append(number_of_start_with_postag("VB", tweet))  # ige
    f.append(number_of_start_with_postag("NN", tweet))  # főnév

    f.extend(get_postag_features(tweet.processed_tags, scorces['pos_pos']))
    f.extend(get_postag_features(tweet.processed_tags, scorces['pos_neg']))

    f.extend(get_bigram_features(tweet.processed_tags, scorces['bi_pos']))
    f.extend(get_bigram_features(tweet.processed_tags, scorces['bi_neg']))

    f.extend(get_trigram_features(tweet.processed_tags, scorces['tri_pos']))
    f.extend(get_trigram_features(tweet.processed_tags, scorces['tri_neg']))

    # f = [float(i) for i in f]
    f.extend(lexicons.getLexiconsFeatures(tweet))

    return f


def num_capitalized_words(tweet):
    return len([word for word in tweet.tokens if word.isupper()])


def num_partlycapitalized_words(tweet):
    return len([word for word in tweet.tokens if (any(x.isupper() for x in word) and any(y.islower() for y in word))])


def num_elongted_words(tweet):
    elong = re.compile("([a-zA-Z])\\1{2,}")
    return len([word for word in tweet.text.split() if elong.search(word)])


def num_urls(tweet):
    return len([word for word in tweet.tokens if word == 'URL'])


def number_of_character(tweet, char):
    return tweet.text.count(char)


def number_of_only_character(tweet, char):
    x = 0
    for token in tweet.tokens:
        if token.count(char) == len(token):
            x += 1

    return x


def number_of_postag(tag, tweet):
    return len([x for x in tweet.postags if x[1] == tag])


def number_of_start_with_postag(tag, tweet):
    return len([x for x in tweet.postags if x[1].startswith(tag)])


def get_postag_features(pos, scorces):
    f = []
    avg, maximum, minimum = F1PosTagsScore(pos, scorces)
    f.append(avg)
    f.append(maximum)
    f.append(minimum)
    return f


def get_bigram_features(pos, scorces):
    f = []
    avg, maximum, minimum = F1PosBigramsScore(pos, scorces)
    f.append(avg)
    f.append(maximum)
    f.append(minimum)
    return f


def get_trigram_features(pos, scorces):
    f = []
    avg, maximum, minimum = F1PosTrigramsScore(pos, scorces)
    f.append(avg)
    f.append(maximum)
    f.append(minimum)
    return f


def calculate_confidences(features, sentiments, C):
    train_confidence = []
    # confidence scores for training data are computed using K-fold cross validation
    kfold = KFold(features.shape[0], n_folds=10)

    for train_index, test_index in kfold:
        X_train, X_test = features[train_index], features[test_index]
        y_train, y_test = sentiments[train_index], sentiments[test_index]

        # train classifier for the subset of train data
        m = train_svm(X_train, y_train, c=C, k="linear")

        # predict confidence for test data and append it to list
        conf = m.decision_function(X_test)
        for x in conf:
            train_confidence.append(x)
    return train_confidence
