import re
import multiprocessing
import nltk
from nltk import word_tokenize, PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from PostagUtils import *


class Preprocessor(object):
    mstopwords = set()
    num_cores = 1

    def __init__(self):
        self.mstopwords = set(stopwords.words('english'))
        self.num_cores = multiprocessing.cpu_count()
        print 'Cores: ' + str(self.num_cores)

    def preprocess_tweets(self, tweets):
        for tweet in tweets:
            tw = self.process(tweet)
            if tw is None:
                tweets.remove(tweet)
        set_bigrams(tweets)
        set_trigrams(tweets)

    def process(self, tweet):
        try:
            text = tweet.origin
            text = text.strip('\'"')
            text = self.remove_whitespaces(text)
            text = self.remove_retweets(text)
            text = self.remove_urls(text)

            tokens = word_tokenize(text)
            tokens = self.remove_stopwords(tokens)
            tokens = self.remove_single_characters(tokens, '@')
            tokens = self.lemmmatizing(tokens)
            word_tokens = []
            for w in tokens:
                if w in self.mstopwords:
                    continue
                else:
                    word_tokens.append(w)

            postags = nltk.pos_tag(tokens)
            tweet.setText(text)
            tweet.setTokens(word_tokens)
            tweet.setPostags(postags)
            self.preprocess_postags(tweet)
            return tweet
        except Exception as e:
            return None

    def remove_whitespaces(self, tweet):
        return re.sub('[\s]+', ' ', tweet)

    def remove_retweets(self, tokens):
        return re.sub('RT @.{0,}: ', '', tokens)

    def remove_urls(self, tokens):
        return re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', tokens)

    def remove_hashtags(self, tweet):
        return re.sub(r'#([^\s]+)', r'\1', tweet)

    def remove_single_characters(self, tokens, char):
        return [w for w in tokens if w is not char]

    def get_processed_texts(self, tweets):
        tweet_texts = []
        for tweet in tweets:
            tweet_texts.append(tweet.text)
        return tweet_texts

    def get_processed_sentiments(self, tweets):
        tweet_sentiments = []
        for tweet in tweets:
            tweet_sentiments.append(int(tweet.sentiment))
        return tweet_sentiments

    def remove_stopwords(self, word_tokens):
        processed = [w for w in word_tokens if not w in self.mstopwords]
        return processed

    def replace_two_or_more(self, tweet):
        pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
        return pattern.sub(r"\1\1", tweet)

    def stemming(self, tokens):
        ps = PorterStemmer()
        processed = []
        for w in tokens:
            if not w[0].isupper():
                processed.append(ps.stem(w))
            else:
                processed.append(w)

        return processed

    def lemmmatizing(self, tokens):
        lemmatizer = WordNetLemmatizer()
        processed = []
        for w in tokens:
            if not w[0].isupper():
                processed.append(lemmatizer.lemmatize(w))
            else:
                processed.append(w)

        return processed

    def preprocess_postags(self, tweet):
        tags = []
        for t in tweet.postags:
            tags.append(t[1] if len(t[1]) < 3 else t[1][:2])
        tweet.setProcessedPostags(tags)
