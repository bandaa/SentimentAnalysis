import nltk
import pickle
import os
import numpy as np
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from nltk.classify import ClassifierI
from statistics import mode
from sklearn import svm
from Tweet import check_tweets2

informative_feature = 20


class NewSitterClassifier(object):
    min_conf = -0.2
    max_conf = 0.2
    classifiers = []

    def __init__(self, classifiers, needbounds=True):
        self.classifiers = []
        if not needbounds:
            self.min_conf = 0
            self.max_conf = 0
        for c in classifiers:
            if c is not None:
                self.classifiers.append(c)

    def get_confidence(self, tweet):
        confidences = []
        for classifier in self.classifiers:
            confidences.append(get_confidence(classifier, tweet))

        confidence = np.bincount(confidences).argmax()
        tweet.setConfidence(confidence)
        if tweet.confidence < self.min_conf:
            tweet.setPrediction(0)
        if tweet.confidence > self.max_conf:
            tweet.setPrediction(1)
        return tweet

    def get_confidences(self, features, tweets):
        confidences = []

        for classifier in self.classifiers:
            confidences.append(get_confidences(classifier, features))

        unprocessed = []
        for i in range(0, len(tweets)):
            try:
                confs = []
                for c in confidences:
                    if c[i] < self.min_conf:
                        confs.append(0)
                    elif c[i] > self.max_conf:
                        confs.append(1)
                    else:
                        pass
                if len(confs) == 0:
                    unprocessed.append(tweets[i])
                    continue

                conf = np.bincount(confs).argmax()
                tweets[i].setConfidence(conf)
                if tweets[i].confidence == 0:
                    tweets[i].setPrediction(0)
                elif tweets[i].confidence == 1:
                    tweets[i].setPrediction(1)
                else:
                    unprocessed.append(tweets[i])
            except Exception as e:
                pass

        print 'deleteing %d tweets, because of the small confidence source' % len(unprocessed)
        for bad_tw in unprocessed:
            tweets.remove(bad_tw)
        check_tweets2(tweets)


def get_confidences(model, features):
    confidences = model.decision_function(features)
    return confidences


def get_confidence(model, tweet):
    two_d_features = []
    two_d_features.append(tweet.features)
    confidence = model.decision_function(two_d_features)
    return confidence


def train_svm(features, sentiments, g="auto", c=1, k="linear", coef0=0, degree=2):
    # define classifier
    if k == "linear":
        model = svm.LinearSVC(C=c, class_weight="balanced")
    elif k == "poly":
        model = svm.SVC(C=c, kernel=k, degree=degree, coef0=coef0)
    elif k == "rbf":
        model = svm.SVC(C=c, kernel=k, gamma=g, class_weight="balanced", cache_size=1000)
    else:
        raise Exception("missing kerner parameter for training")

    # fit data
    model.fit(features, sentiments)

    return model


def clear_model(name):
    try:
        os.remove("Models/" + name + '.pickle')
    except Exception:
        pass


def save_model(model, name):
    file = open("Models/" + name + '.pickle', "wb")
    pickle.dump(model, file)
    file.close()
    print 'Model ' + name + ' saved'


def open_model(name):
    try:
        file = open("Models/" + name + '.pickle', "rb")
        model = pickle.load(file)
        file.close()
        print 'Model' + name + " opened"
        return model
    except IOError:
        pass
