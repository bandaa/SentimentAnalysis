import sys
import time
import datetime
from Preprocessor import Preprocessor
from SitterLexicons import Lexicons
from Tweet import convert_tweet_to_stweet, check_tweets, check_tweet, check_tweet2, get_all_feature_asnp, \
    get_all_sentiment_asnp
from Utils import load_train_tweets
from NewSitterFeatures import getFeatures, calculate_features
from SitterClassifier import train_svm, save_model, open_model, get_confidence, NewSitterClassifier, clear_model
from SitterReview import save_traning_statistics, setup_tweets_statistic, \
    save_tweets_statistics, test_accuracy2, save_twitter_query_stat, get_current_model_id, count_posneg
from SitterVisualizer import drawPie, drawKernel


class Sitter(object):

    C1 = 0
    C2 = 0
    C3 = 0
    classifier_name = "Sitter_SVM_classifier"
    version = "cTest " + str(C1) + "," + str(C2) + "," + str(C3)
    statistic = {}
    # TODO catch all exceptions and save them to statistic
    error_s = ''

    def __init__(self):
        print 'Setting up sitter..'
        self.preprocessor = Preprocessor()
        self.lexicons = Lexicons()
        self.lexicons.load()
        self.base_start = time.time()
        self.classifier = None

    def set_cparams(self, c1, c2, c3):
        self.C1 = c1
        self.C2 = c2
        self.C3 = c3
        self.version = "cTest " + str(c1) + "," + str(c2) + "," + str(c3)

    def setup_query(self, title, count):
        self.query = title
        self.query_count = count
        self.processed = []

    def featurizing(self, data):
        start_length = len(data)
        self.preprocessor.preprocess_tweets(data)

        processed_length = len(data)
        print "%d / %d tweets processed" % (processed_length, start_length)

        getFeatures(data, self.lexicons)
        features_length = len(data)

        print "%d / %d features created" % (features_length, processed_length)

        check_tweets(data)
        checked_length = len(data)
        print "%d / %d tweet checked" % (checked_length, features_length)

    def tweet_featurizing(self, tweet):
        try:
            tweet = self.preprocessor.process(tweet)
            if tweet is None:
                return None
            features = calculate_features(tweet, self.lexicons)
            tweet.setFeatures(features)
            if features is None:
                return None
            tweet = check_tweet(tweet)
            if tweet is None:
                return None
            return tweet
        except Exception as e:
            return None

    def train(self, limit):
        start = time.time()
        print 'Sitter training started'

        tweets = load_train_tweets(limit)
        train_data = []
        for d in tweets:
            train_data.append(convert_tweet_to_stweet(d))

        train_length = len(train_data)
        print '%d / %d training tweets loaded' % (train_length, limit)
        self.featurizing(train_data)

        allsentiment = get_all_sentiment_asnp(train_data)
        allfeature = get_all_feature_asnp(train_data)


        #allfeature = regularize(allfeature)
        #refreshFeatures(train_data, allfeature)

        # FIXME we do not need to calculate while training (only in test phase)
        # confidences = calculate_confidences(allfeature, allsentiment, C1)
        #confidences = normalize(confidences)
        #refreshConfidences(train_data, confidences)
        # print '%d confidence calculated ' % len(confidences)

        # final_list = concatenate_final_set(train_data)
        models = []
        clear_model(self.classifier_name + "0")
        if self.C1 != 0:
            svm_model0 = train_svm(allfeature, allsentiment, c=self.C1, k="linear")
            models.append(svm_model0)
            save_model(svm_model0, self.classifier_name + "0")
        clear_model(self.classifier_name + "1")
        if self.C2 != 0:
            svm_model1 = train_svm(allfeature, allsentiment, c=self.C2, k="rbf")
            models.append(svm_model1)
            save_model(svm_model1, self.classifier_name + "1")
        clear_model(self.classifier_name + "2")
        if self.C3 != 0:
            svm_model2 = train_svm(allfeature, allsentiment, c=self.C3, k="poly")
            models.append(svm_model2)
            save_model(svm_model2, self.classifier_name + "2")
        mclassifier = NewSitterClassifier(models)

        print '%s model trained' % self.classifier_name

        self.classifier = mclassifier
        end = time.time()
        print 'Sitter traning ended in %d sec' % (end - start)
        self.statistic.update(
            {'Date': datetime.datetime.now(), 'Version': self.version, 'Train size': limit, 'Train time': (end - start),
             'Train rate': ("%d:%d" % (len(train_data), limit))})
        drawKernel(allfeature, allsentiment, self.version) # not work well.. :(

    def test(self, limit):
        start = time.time()
        print 'Sitter testing started'

        tweets = load_train_tweets(limit)
        test_data = []
        for d in tweets:
            test_data.append(convert_tweet_to_stweet(d))

        test_length = len(test_data)
        print '%d / %d testing tweets loaded' % (test_length, limit)
        self.featurizing(test_data)
        test_length = len(test_data)

        all_feature = get_all_feature_asnp(test_data)
        #all_feature = regularize(all_feature)
        #refreshFeatures(test_data, all_feature)

        if self.classifier is None:
            print 'Classifier not ready, trying to set up from pickles'
            sitter_svm0 = open_model(self.classifier_name+"0")
            sitter_svm1 = open_model(self.classifier_name+"1")
            sitter_svm2 = open_model(self.classifier_name+"2")

            self.classifier = NewSitterClassifier([sitter_svm0, sitter_svm1, sitter_svm2])

        self.classifier.get_confidences(all_feature, test_data)
        confidence_length = len(test_data)

        print "%d / %d tweet confidence calculated" % (confidence_length, test_length)

        matched, missed = test_accuracy2(test_data)
        pos, neg = count_posneg(test_data)
        test_length = len(test_data)

        print "%d / %d test ended" % (test_length, confidence_length)
        accuracy = (float(matched) / float(test_length))
        print "%f percent accuracy detected for %d test data" % (accuracy, test_length)
        print "%d positive and %d negative tweets founded" % (pos, neg)
        end = time.time()

        self.statistic.update({'Test size': limit, 'Test time': (end - start), 'Test accuracy': accuracy,
                               'Test rate': ("%d:%d" % (len(test_data), limit))})
        setup_tweets_statistic()
        for tweet in test_data:
            save_tweets_statistics(tweet.get_as_dictionary())

    def detect_sentiment(self, tweet, counter):
        print '%d: start sentiment detection: %s' % (counter, tweet.origin[:10])
        start = time.time()
        tweet = self.tweet_featurizing(tweet)
        if tweet is None:
            return None
        tweet = get_confidence(self.classifier, tweet)
        tweet = check_tweet2(tweet)

        if tweet is None:
            return None

        self.processed.append(tweet)
        print '%d: successfully ended: sentiment: %f tweet: %s' % (counter, tweet.confidence, tweet.text[:10])
        return tweet

    def detect_sentiments(self, tweets, needbounds = True):
        start = time.time()
        print "Sitter sentiments detection started"
        self.featurizing(tweets)

        all_feature = get_all_feature_asnp(tweets)
        if self.classifier is None:
            print 'Classifier not ready, trying to set up from pickles'
            sitter_svm0 = open_model(self.classifier_name+"0")
            sitter_svm1 = open_model(self.classifier_name+"1")
            sitter_svm2 = open_model(self.classifier_name+"2")

            self.classifier = NewSitterClassifier([sitter_svm0, sitter_svm1, sitter_svm2], needbounds)

        self.classifier.get_confidences(all_feature, tweets)
        self.processed.extend(tweets)
        print "Sitter sentiment detection ended: %d sec" % (time.time() - start)

    def save_sitter_statistic(self):
        self.base_end = time.time()
        all_run = self.base_end - self.base_start
        self.statistic.update({'all run': all_run})
        save_traning_statistics(self.statistic)
        print 'Sitter ended at: %d sec' % all_run

    def save_stream_data(self, time):
        colors = ['green', 'red']
        labels = ['positive', 'negative']
        neg = 0
        pos = 0
        if self.processed is None:
            print 'there is no analised tweet, could not create statistic'

        setup_tweets_statistic()
        for p in self.processed:
            if hasattr(p, 'prediction'):
                if p.prediction == 0:
                    neg += 1
                if p.prediction == 1:
                    pos += 1
            save_tweets_statistics(p.get_as_dictionary())
        print '%d positive and %d negative tweet founded from %d/%d processed tweet for query %s' % (
        pos, neg, len(self.processed), self.query_count, self.query)
        sizes = [pos, neg]
        save_twitter_query_stat({"Date": datetime.datetime.now(), "Query": self.query, "Positive": pos, "Negative": neg,
                                 "Successed": len(self.processed), "Limit": self.query_count, "Time": time,
                                 "ModelId": get_current_model_id()})

        drawPie(colors, sizes, labels, "Sentiment of {} Tweets about {}".format(len(self.processed), self.query))


def test_sitter_cparams():
    c1_param = [0, 0.001, 0.01, 0.1, 0.5, 1.0]
    c2_param = [0, 0.001, 0.01, 0.1, 0.5, 1.0]
    c3_param = [0, 0.001, 0.01, 0.1, 0.5, 1.0]

    counter = 0
    for c1 in c1_param:
        for c2 in c2_param:
            for c3 in c3_param:
                try:
                    if c1 == 0 and c2 == 0 and c3 == 0:
                        continue
                    print "version %f, %f, %f started.." % (c1, c2, c3)
                    sitter = Sitter()
                    sitter.set_cparams(c1, c2, c3)
                    sitter.train(1000)
                    sitter.test(1000)
                    sitter.save_sitter_statistic()
                    print "version %f, %f, %f successed " % (c1, c2, c3)
                    counter += 1
                except Exception as e:
                    print "version %f, %f, %f failed because: %s" % (c1, c2, c3, e.message)

if __name__ == "__main__":
    #for i in range(0, 5):
    #    test_sitter_cparams()
    best_c1 = 0.1
    best_c2 = 0.001
    best_c3 = 0

    sitter = Sitter()
    sitter.set_cparams(best_c1, best_c2, best_c3)
    sitter.train(100000)
    sitter.test(10000)
    sitter.save_sitter_statistic()
    print "cy@"

