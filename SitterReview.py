import nltk
import csv
import datetime

stat_field_names = ['Id', 'Date', 'Version', 'Train size', 'Train time', 'Train rate', 'Test size', 'Test time',
                    'Test accuracy', 'Test rate', 'Error', 'Comment', 'all run']
stat_file_name = "Statistics/Model/statistic.csv"

result_field_names = ['Origin', 'Sentiment', 'Prediction', 'Confidence', 'Features', 'Tokens', 'Processed']
result_file_name = "Statistics/Tweets/statistic_" + str(datetime.datetime.now().strftime('%Y-%m-%d-%H_%M')) + ".csv"

query_statistic = ['Date', 'Query', 'Positive', 'Negative', 'Successed', 'Limit', 'Time', 'ModelId']
query_statistic_filename = "Statistics/Model/twitter_statistic.csv"


def test_accuracy2(tweets):
    match = 0
    missed = 0
    for tw in tweets:
        try:
            if int(tw.sentiment) == tw.prediction:
                match += 1
            else:
                missed += 1
        except Exception as e:
            tweets.remove(tw)
    return match, missed


def setup_tweets_statistic():
    with open(result_file_name, "w") as file:
        writer = csv.DictWriter(file, fieldnames=result_field_names)
        writer.writeheader()
    print 'Tweets statistic is ready!'


def save_traning_statistics(training_dict):
    id = int(get_current_model_id()) + 1
    with open(stat_file_name, 'a') as file:
        training_dict.update({'Id': id})
        writer = csv.DictWriter(file, fieldnames=stat_field_names)
        writer.writerow(training_dict)
    print 'Traning_statistics updated'


def get_current_model_id():
    with open(stat_file_name, 'r') as file:
        return len(file.readlines())


def save_tweets_statistics(tweets_dict):
    try:
        with open(result_file_name, 'a') as file:
            writer = csv.DictWriter(file, fieldnames=result_field_names)
            writer.writerow(tweets_dict)
    except Exception as e:
        print e.message
        pass


def save_twitter_query_stat(query_dict):
    with open(query_statistic_filename, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=query_statistic)
        writer.writerow(query_dict)
    print 'Twitter query statistic saved'


def count_posneg(tweets):
    pos = 0
    neg = 0
    for t in tweets:
        if t.prediction == 0:
            neg += 1
        elif t.prediction == 1:
            pos += 1
    return pos, neg
