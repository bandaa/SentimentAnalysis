import csv
import sys
import time
from random import random, shuffle
from Tweet import *

# just an upper estimation (current training size: 1578628)
upper = 2000000.0
# set this (optimal 1-10) to regulate the randomness of training set (1-better, 10-faster)
perfect_selection = 10


def load_train_tweets(limit):
    start = time.time()
    selection_prop = (limit / upper) * perfect_selection
    if selection_prop > 1:
        selection_prop = 1
    if selection_prop < 0:
        raise Exception('training size is to small..')
    print "Selection probability = %f" % selection_prop
    tweets = []
    pos_counter = 0
    neg_counter = 0
    while True:
        # train2 csv is also valid
        with open('Source/train.csv', 'rb') as csv_file:
            data_source = csv.DictReader(csv_file)
            for tweet in data_source:
                if random() < selection_prop:
                    try:
                        if tweet['Sentiment'] == '0' and neg_counter < (limit / 2):
                            tweets.append(Tweet(neg_counter + pos_counter, tweet['SentimentText'].encode('utf-8'),
                                                '0'))
                            neg_counter += 1

                        if (tweet['Sentiment'] == '1' or tweet['Sentiment'] == '4') and pos_counter < (limit / 2):
                            tweets.append(Tweet(neg_counter + pos_counter, tweet['SentimentText'].encode('utf-8'),
                                                '1'))
                            pos_counter += 1

                        if (pos_counter + neg_counter) >= limit:
                            shuffle(tweets)
                            check_input_polarity(tweets)
                            print "loading time: %d " % (time.time() - start)
                            return tweets
                    except:
                        pass


def check_input_polarity(tweets):
    pos = 0
    neg = 0
    for tweet in tweets:
        if int(tweet.sentiment) == 0:
            neg += 1
        elif int(tweet.sentiment) == 1:
            pos += 1
        else:
            print "Invalid input sentiment, shout down Sitter"
            exit(0)
    if pos != neg:
        print "Unbalanced sitter input, shout down Sitter"
        exit(0)
    else:
        print "Input balance ok"


def load_text_from_args():
    if len(sys.argv) <= 1:
        print "Usage : python sitter.py \"message1\" \"message2\" ..."
        sys.exit(0)
    else:
        user_input = sys.argv[1:]
        for i in range(0, len(user_input)):
            try:
                user_input[i] = user_input[i].decode('utf8')
            except:
                user_input[i] = unicode(user_input[i], errors='replace')

    messages_test = []
    for message in user_input:
        if message.strip() != '':
            messages_test.append(message.strip() + "\n")
    return messages_test


def load_tweet_from_args():
    if len(sys.argv) <= 1:
        print "Usage : python sitter.py \"message1\" \"message2\" ..."
        sys.exit(0)
    else:
        user_input = sys.argv[1:]
        for i in range(0, len(user_input)):
            try:
                user_input[i] = user_input[i].decode('utf8')
            except:
                user_input[i] = unicode(user_input[i], errors='replace')

    tweets = []
    for message in user_input:
        if message.strip() != '':
            tweets.append(Tweet(None, message.strip() + "\n", None))
    return tweets


def load_command_from_args():
    if len(sys.argv) <= 1:
        print 'Please specify the command'
        sys.exit(0)
    else:
        return sys.argv[1]
