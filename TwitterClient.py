import json
import sys
import time
import tweepy
from Tweet import STweet
from NewSitter import Sitter

# keys and tokens from the Twitter Dev Console
consumer_key = 'KgQ4oOwSgAcMZaVGcBL6p0Lv6'
consumer_secret = 'eR2qgMEbe1E4RH9nEMjzGzxiwb2ca062ENlZxGmyvP1GZwFh2h'
access_token = '730111874356936704-jT3QxwTJHQ5MMP5TA2b2W4yYWZgr5bB'
access_token_secret = 'qjRVtUcaWxmj6QMx0t52DmdtwWYmvvLAUw1SXLOORcAER'

class listener(tweepy.StreamListener):

    tweets = []

    def __init__(self, limit, query, api):
        self.api = api
        self.limit = limit
        self.counter = 0
        self.sitter = Sitter()
        self.sitter.setup_query(query, limit)
        self.starttime = time.time()

    def on_status(self, status):
        tweet = STweet()
        try:
            # FIXME: im not sure.. the format of the tweets are very various
            try:
                text = status.extended_tweet["full_text"].decode('utf-8')
            except AttributeError:
                try:
                    text = status.retweeted_status.extended_tweet['full_text'].decode('utf-8')
                except AttributeError:
                    text = status.text.decode('utf-8')

            tweet.setOriginText(text)
            self.tweets.append(tweet)
            print "%d/%d Tweet loaded: %s " % (len(self.tweets), self.limit, text[:256])
        except Exception as e:
            # skip if error, download new!
            return

        if len(self.tweets) >= self.limit:
            print 'Twitter stream reached limit %d, SHUT DOWN connection' % self.limit
            self.end = time.time()
            self.sitter.detect_sentiments(self.tweets)
            self.sitter.save_stream_data(self.end-self.starttime)
            exit(0)

    def on_error(self, status):
        print status

    def on_exception(self, exception):
        print exception.message


def load_twitter_data():
    query = sys.argv[1]
    limit = int(sys.argv[2])
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    print "Twitter stream started.."
    l = listener(limit, query, api)
    twitter_stream = tweepy.Stream(api.auth, l)
    twitter_stream.filter(track=[query], async=True)

load_twitter_data()