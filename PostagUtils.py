import os
import time
import pickle
from nltk import bigrams
from nltk import trigrams


def set_bigrams(tweets):
    for tweet in tweets:
        try:
            tweet.setBigrams(list(bigrams(tweet.tokens)))
        except:
            tweets.remove(tweet)


def set_trigrams(tweets):
    for tweet in tweets:
        try:
            tweet.setTrigrams(list(trigrams(tweet.tokens)))
        except:
            tweets.remove(tweet)


def get_bigramms(tweets, category=None):
    bi = []
    for tweet in tweets:
        if category is None:
            bi.append(list(bigrams(tweet.processed_tags)))
        else:
            if int(tweet.sentiment) == category:
                bi.append(list(bigrams(tweet.processed_tags)))
    return bi


def get_trigrams(tweets, category=None):
    tri = []
    for tweet in tweets:
        if category is None:
            tri.append(list(trigrams(tweet.processed_tags)))
        else:
            if int(tweet.sentiment) == category:
                tri.append(list(trigrams(tweet.processed_tags)))
    return tri


def get_unique_postags(tweets, category=None):
    unique = set()
    for tweet in tweets:
        if category is None:
            for tag in tweet.postags:
                unique.add(tag[1])
        elif category == int(tweet.sentiment):
            for tag in tweet.postags:
                unique.add(tag[1])
    return list(unique)


def get_unique_bigrams(bigrams):
    unique = set()
    for bi in bigrams:
        for b in bi:
            unique.add(b)
    return unique


def get_unique_trigrams(trigrams):
    unique = set()
    for tri in trigrams:
        for tr in tri:
            unique.add(tr)
    return unique


def get_all_tag(tweets):
    sub = []
    for tw in tweets:
        sub.append(tw.processed_tags)
    return sub


def get_selected_category(tweets, category=None):
    sublist = []
    for tw in tweets:
        if category is not None and int(tw.sentiment) == category:
            sublist.append(tw)
    return sublist


def get_selected_category_tags(tweets, label):
    sublist = []
    for tw in tweets:
        if int(tw.sentiment) == label:
            sublist.append(tw.processed_tags)
    return sublist


def f1_bigram_score(bigram, bigrams_in_category, all_bigrams):
    x1 = 0
    for i in range(0, len(bigrams_in_category)):
        if bigram in bigrams_in_category[i]:
            x1 = + 1
    x2 = 0
    for i in range(0, len(all_bigrams)):
        if bigram in all_bigrams[i]:
            x2 += 1

    x3 = len(bigrams_in_category)
    if x2 == 0:
        precision = 0
    else:
        precision = x1 / float(x2)

    recall = x1 / float(x3)
    if precision == 0 or recall == 0:
        return 0
    else:
        return (2 * precision * recall) / float(precision + recall)


def score_postag(tweets):
    negative = get_selected_category_tags(tweets, 0)
    postive = get_selected_category_tags(tweets, 1)
    all = get_all_tag(tweets)

    unique_neg_tags = get_unique_postags(get_selected_category(tweets, 0))
    unique_pos_tags = get_unique_postags(get_selected_category(tweets, 1))

    d_pos = {}
    d_neg = {}
    for t in unique_neg_tags:
        d_neg[t] = f1_bigram_score(t, negative, all)

    for t in unique_pos_tags:
        d_pos[t] = f1_bigram_score(t, postive, all)

    return d_pos, d_neg


def score_bigrams(tweets):
    positive = get_bigramms(tweets, 1)
    negative = get_bigramms(tweets, 0)
    all = get_bigramms(tweets)

    unique_pos_bigrams = get_unique_bigrams(positive)
    unique_neg_bigrams = get_unique_bigrams(negative)

    d_pos = {}
    d_neg = {}

    for t in unique_neg_bigrams:
        d_neg[t] = f1_bigram_score(t, negative, all)

    for t in unique_pos_bigrams:
        d_pos[t] = f1_bigram_score(t, positive, all)

    return d_pos, d_neg


def score_trigrams(tweets):
    positive = get_trigrams(tweets, 1)
    negative = get_trigrams(tweets, 0)
    all = get_trigrams(tweets)

    unique_pos_trigrams = get_unique_trigrams(positive)
    unique_neg_trigrams = get_unique_trigrams(negative)

    d_pos = {}
    d_neg = {}

    for t in unique_neg_trigrams:
        d_neg[t] = f1_bigram_score(t, negative, all)

    for t in unique_pos_trigrams:
        d_pos[t] = f1_bigram_score(t, positive, all)

    return d_pos, d_neg


def getScorces(tweets):
    starttime = time.time()
    print "start calculation f1 scores.."
    postag_pos, postag_neg = score_postag(tweets)
    bigrams_pos, bigrams_neg = score_bigrams(tweets)
    trigrams_pos, trigrams_neg = score_trigrams(tweets)

    print "f1 scorces calculated at: %d sec" % (time.time() - starttime)
    return {'pos_pos': postag_pos, 'pos_neg': postag_neg, 'bi_pos': bigrams_pos, 'bi_neg': bigrams_neg,
            'tri_pos': trigrams_pos, 'tri_neg': trigrams_neg}


def saveScores(source):
    with open('Models/Scorces/scores.pkl', 'wb') as output:
        pickle.dump(source['pos_pos'], output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(source['pos_neg'], output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(source['bi_pos'], output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(source['bi_neg'], output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(source['tri_pos'], output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(source['tri_neg'], output, pickle.HIGHEST_PROTOCOL)
    print "Scores saved"


def loadScores():
    scorces = {}
    with open('Models/Scorces/scores.pkl', 'rb') as input:
        scorces['pos_pos'] = pickle.load(input)
        scorces['pos_neg'] = pickle.load(input)
        scorces['bi_pos'] = pickle.load(input)
        scorces['bi_neg'] = pickle.load(input)
        scorces['tri_pos'] = pickle.load(input)
        scorces['tri_neg'] = pickle.load(input)
    print "Scores loaded"
    return scorces


def clearScores():
    try:
        os.remove("Models/Scorces/scores.pkl")
        print "Scores removed"
    except Exception:
        pass


def F1PosTagsScore(pos, pos_tags_scores):
    scores = []

    for x in pos:
        scores.append(pos_tags_scores.get(x, 0))

    try:
        average = sum(scores) / float(len(scores))
    except:
        average = 0
    try:
        maximum = max(scores)
    except:
        maximum = 0
    try:
        minimum = min(scores)
    except:
        minimum = 0

    return average, maximum, minimum


def F1PosBigramsScore(pos, pos_bigrams_scores):
    scores = []

    for x in pos:
        scores.append(pos_bigrams_scores.get(x, 0))

    try:
        average = sum(scores) / float(len(scores))
    except:
        average = 0
    try:
        maximum = max(scores)
    except:
        maximum = 0
    try:
        minimum = min(scores)
    except:
        minimum = 0

    return average, maximum, minimum


def F1PosTrigramsScore(pos, pos_trigrams_scores):
    scores = []

    for x in pos:
        scores.append(pos_trigrams_scores.get(x, 0))

    try:
        average = sum(scores) / float(len(scores))
    except:
        average = 0

    try:
        maximum = max(scores)
    except:
        maximum = 0
    try:
        minimum = min(scores)
    except:
        minimum = 0

    return average, maximum, minimum
